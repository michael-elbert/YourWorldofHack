var PERM, Permissions, __indexOf = [].indexOf || function(item) {
    for (var i = 0, l = this.length; i < l; i++) {
        if (i in this && this[i] === item) return i;
    }
    return -1;
};
PERM = {
    PUBLIC: 0,
    MEMBERS: 1,
    ADMIN: 2
};
Permissions = {
    can_admin: function(user, world) {
        if (!user.authenticated) {
            return false;
        }
        if (user.is_superuser) {
            return true;
        }
        return user.id === world.owner_id;
    },
    user_matches_perm: function(user, world, perm) {
        var _ref;
        if (perm === PERM.PUBLIC) {
            return true;
        }
        if (!user.authenticated) {
            return false;
        }
        if (Permissions.can_admin(user, world)) {
            return true;
        }
        if (perm === PERM.ADMIN) {
            return false;
        }
        assert(perm === PERM.MEMBERS);
        return _ref = world.id, __indexOf.call(user.whitelists, _ref) >= 0;
    },
    can_read: function(user, world) {
        return Permissions.user_matches_perm(user, world, world.readability);
    },
    can_write: function(user, world) {
        if (!Permissions.can_read(user, world)) {
            return false;
        }
        return Permissions.user_matches_perm(user, world, world.writability);
    },
    can_edit_tile: function(user, world, tile) {
        if (!tile.initted()) {
            throw new Error("Can't check perms on un-initted tile");
        }
        if (!Permissions.can_read(user, world)) {
            return false;
        }
        if (tile.writability === null) {
            return Permissions.can_write(user, world);
        }
        return Permissions.user_matches_perm(user, world, tile.writability);
    },
    can_protect_tiles: function(user, world) {
        var _ref;
        if (Permissions.can_admin(user, world)) {
            return true;
        }
        return world.feature_membertiles_addremove && (_ref = world.id, __indexOf.call(user.whitelists, _ref) >= 0);
    },
    can_coordlink: function(user, world, opt_tile) {
        var action, write;
        if (opt_tile) {
            write = Permissions.can_edit_tile(user, world, opt_tile);
        } else {
            write = Permissions.can_write(user, world);
        }
        action = Permissions.user_matches_perm(user, world, world.feature_coord_link);
        return write && action;
    },
    can_urllink: function(user, world, opt_tile) {
        var action, write;
        if (opt_tile) {
            write = Permissions.can_edit_tile(user, world, opt_tile);
        } else {
            write = Permissions.can_write(user, world);
        }
        action = Permissions.user_matches_perm(user, world, world.feature_url_link);
        return write && action;
    },
    can_go_to_coord: function(user, world) {
        return true;
    },
    can_paste: function(user, world) {
        return true;
    },
    get_perm_display: function(permission) {
        return {
            0: 'PUBLIC',
            1: 'MEMBERS',
            2: 'ADMIN'
        }[permission];
    }
};