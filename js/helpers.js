var DEBUG,Helpers,assert,log,__slice=[].slice;DEBUG=false;assert=function(exp,opt_msg){if(!exp){throw new Error(opt_msg||"Assertion failed");}};log=function(){if(DEBUG&&console&&console.log){return console.log.apply(console,arguments);}};Helpers={escapeChar:function(s){if(s==="<"){return"&lt;";}
if(s===">"){return"&gt;";}
if(s==="&"){return"&amp;";}
if(s===" "){return"&nbsp;";}
return s;},addCss:function(cssCode){var styleElement;styleElement=document.createElement("style");styleElement.type="text/css";if(styleElement.styleSheet){styleElement.styleSheet.cssText=cssCode;}else{styleElement.appendChild(document.createTextNode(cssCode));}
document.getElementsByTagName("head")[0].appendChild(styleElement);return styleElement;},_getNodeIndex:function(node){return $(node).parent().children().index(node);},getCellCoords:function(td){var charX,charY,tile,tileX,tileY;td=$(td);charX=Helpers._getNodeIndex(td);charY=Helpers._getNodeIndex(td.parents("tr"));tile=td.parents(".tilecont")[0];tileY=$.data(tile,"tileY");tileX=$.data(tile,"tileX");return[tileY,tileX,charY,charX];},vectorLen:function(){var sum,x,_i,_len;sum=0;for(_i=0,_len=arguments.length;_i<_len;_i++){x=arguments[_i];sum+=x*x;}
return Math.sqrt(sum);},deepEquals:function(o1,o2){var length,name,t;t=typeof o1;if(t!==typeof o2){return false;}
if(t==='number'||t==='string'||t==='boolean'||t==='function'){return o1===o2;}
if(t==="undefined"){return true;}
if(o1===null||o2===null){return o1===o2;}
length=o1.length;if(length!==o2.length){return false;}
for(name in o1){if(!Helpers.deepEquals(o1[name],o2[name])){return false;}}
for(name in o2){if(!Helpers.deepEquals(o1[name],o2[name])){return false;}}
return true;},charAt:function(s,i){var c,n;c=s.charAt(i);n=s.charAt(i+ 1);if(this.isSurrogate(c,n)){return c+ n;}
return c;},length:function(s){return this.getChars(s).length;},getChars:function(s){var c,i,n,r;r=[];i=-1;while(++i<s.length){c=s.charAt(i);n=s.charAt(i+ 1);if(this.isSurrogate(c,n)){r.push(c+ n);i++;}else{r.push(c);}}
return r;},isSurrogate:function(){var c,d,s,_i,_len;s=1<=arguments.length?__slice.call(arguments,0):[];for(_i=0,_len=s.length;_i<_len;_i++){c=s[_i];d=c.charCodeAt(0);if(d<0xD800||d>0xDFFF){return false;}}
return true;}};
