# Your World of Hack
----
Your World of Hack is a [Your World of Text](http://www.yourworldoftext.com/) client that has numerous modifications, providing a more rich experience.

## Features
 Feature | Implemented?
 :---: | :---: 
Open w variable (makes it 100 times easier to script!) | ✓
Ability to use default paste | ✓
Ability to use default teleport | ✓
Super cool favicon | ✓
Go to any world | ✓*
Unerasable text | ✓(Private)
Smart tile eraser | TODO
Instant teleport | TODO
Drawing | TODO
Comic Sans font | TODO
Highlight and copy text | TODO

*To go to a world other than the main one, type a ? and then the world path 

Examples:

`YourWorldofHack/index.html?forexample` will go to /forexample

`YourWorldofHack/index.html?~InfraRaven/JacobsLadder` will go to /~InfraRaven/JacobsLadder

So far clicking links will take you to the page you clicked _on the official YWoT. In order to go to that world on YWoH, you have to manually type it in the URL._

## Running this for yourself
---
All you have to do to get this running is download the zip, extract it, then open index.html.

## Contributing
---
If you would like to contribute, whether you would like to code for this project or just have a neat idea, contact me through email at [michael.elb3rt@gmail.com](mailto:michael.elb3rt@gmail.com)